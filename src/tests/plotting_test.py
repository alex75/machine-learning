import unittest
from unittest.mock import Mock
import plotting



class plottingTest(unittest.TestCase):

    def test_draw_serie(self):

        x = [1,2,3,4,5,6]
        y = [1.1, 2.2, 3.3, 4.4, 5.5, 4.4]

        values = list(zip(x,y))


        plotting.draw_serie(values, "serie A", plotting.colors.red)


    def test_draw_serie_2(self):

        x = [1,2,3,4,5,6]
        y = [1.1, 2.2, 3.3, 4.4, 5.5, 4.4]

        values = list(zip(x,y))

        plotting.draw_serie(values, "serie A", plotting.colors.red)

