from typing import List

class TrainingSet:

    def __init__(self, training_records:List):
        self.records = training_records


class TrainingData:

    def __init__(self, inputs:List, target:int):
        self.inputs = inputs
        self.target = target