import numpy
import pandas

from machinelearning.perceptron import Perceptron

import sys
sys.path.append("..")  # needed to add a package on a parent folder (plotting)
import plotting

iris_database_url = "http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
# information about the data here: http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.names

def run():

    # parse the CSV file and create a DataFrame without header
    data = pandas.read_csv(iris_database_url, header=None)

    # get the values (1 dimension ndarray) from the fifth column of the records from 0 to 100 ("Iris-setosa" and "Iris-versicolor")
    labels = data.iloc[0:100, 4].values

    # set different labels for the different classes
    labels = numpy.where(labels == "Iris-setosa", 1, labels)
    labels = numpy.where(labels == "Iris-versicolor", -1, labels)
    #labels = numpy.where(labels == "Iris-virginica", 3, labels)

    # get the values (2 dimensions ndarray) for "sepal length" and "petal length" from columns 0 and 2 for the records from 0 to 100
    features = data.iloc[0:100, [0, 2]].values

    # create the Net
    net = Perceptron(learning_rate=0.01, n_iterations=10)

    # start the learning process
    net.fit(features, labels)


    # plotting

    from plotting.graphContainer import GraphsContainer
    graphs_container = GraphsContainer("Machine Learning", 2, 1)
    left_plot = graphs_container.add_graph(1, 1)

    color_setosa = plotting.colors.blue
    color_virginica = plotting.colors.green

    axis_labels = ["sepal length", "petal length"]
    graph = plotting.create_graph(left_plot, axis_labels=axis_labels)
    graph.draw_serie(features[0:50], "Iris-setosa", color_setosa, plotting.markers.circle)
    graph.draw_serie(features[50:100], "Iris-virginica", color_virginica, plotting.markers.triangle)

    bottom_plot = graphs_container.add_graph(2, 1)
    graph_errors = plotting.create_graph(bottom_plot, axis_labels=["Epochs", "Number of misclassifications"])
    errors = list(zip(range(1, len(net.epoch_errors)+1), net.epoch_errors))
    graph_errors.draw(plotting.graph_types.line, errors, "Errors", plotting.colors.red, plotting.markers.point)


    # plot the decision surface
    margin = 0.2
    x_min, x_max = features[:, 0].min()-margin, features[:, 0].max()+margin
    y_min, y_max = features[:, 1].min()-margin, features[:, 1].max()+margin

    resolution = 0.01
    x_values, y_values = numpy.meshgrid(numpy.arange(x_min, x_max, resolution), numpy.arange(y_min, y_max, resolution))
    training = numpy.array([x_values.ravel(), y_values.ravel()]).T
    result = net.predict(training)
    result = result.reshape(x_values.shape)



    colors = [color_virginica, color_setosa]
    plotting.create_contour_graph(left_plot, "Classification areas", axis_labels=axis_labels,
                                                  x_values= x_values, y_values= y_values, z_values=result, colors=colors, alpha=0.2)

    graphs_container.show(True)


def main():
    
    print("Machine Learning")

    run()

    print("end")

    #input("\nPress any key to close...")


if __name__ == '__main__':
    main()

