import numpy
from typing import List
from machinelearning.training import TrainingSet

class Net:

    def __init__(self):
        self.weights = None

    def predict(self, inputs):

        if self.weights is None:
            self.weights = numpy.zeros((len(inputs)))

        net_input = 0
        for x in range(0, len(inputs)):
            net_input += inputs[x] * self.weights[x]
        return 1 if net_input >= 0 else -1

    def train(self, training_set: TrainingSet, learning_rate: float = 0.1, max_iterations: int = 100):
        """
        Apply the delta rule of gradient descent.
        It says: delta weight = error * input * learning-rate
        """

        is_trained = False
        count = 0

        while not is_trained and count < max_iterations:
            errors = 0
            for training in training_set.records:

                output = self.predict(training.inputs)
                error = training.target - output
                if error != 0:
                    errors += 1
                    for x in range(0,len(training.inputs)):
                        update = (training.inputs[x] * error) * learning_rate
                        self.weights[x] += update

            if errors == 0:
                is_trained = True
            count += 1


