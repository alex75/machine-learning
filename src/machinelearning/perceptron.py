import numpy as np

class Perceptron:

    def __init__(self, learning_rate = 0.01, n_iterations = 10):
        self.learning_rate = learning_rate
        self.n_iterations = n_iterations

    def fit(self, training_set, target_values):
        """ Fit training data """

        self.weights = np.zeros(1 + training_set.shape[1])
        self.epoch_errors = []

        for i in range(self.n_iterations):
            errors = 0
            for xi, target in zip(training_set, target_values):
                update = self.learning_rate * (target - self.predict(xi))
                self.weights[1:] += update * xi
                self.weights[0] += update
                errors += int(update != 0.0)
            self.epoch_errors.append(errors)

        return self

    def net_input(self, training_set):
        """ Calculate net input """
        return np.dot(training_set, self.weights[1:]) + self.weights[0]

    def predict(self, training_set):
        """ Return class label after unit step """
        return np.where(self.net_input(training_set) >= 0.0, 1, -1)