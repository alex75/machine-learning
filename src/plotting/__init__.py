from typing import List

from plotting.graph import Graph
from plotting.colors import *
from plotting.markers import *


class GraphTypes:
    points = 1
    line = 2

graph_types = GraphTypes()


def create_graph(plot=None, window_name:str=None, axis_labels:List[str]=None):

    return graph.Graph(plot=plot, window_name=window_name, axis_labels=axis_labels)


def draw(type_:int, values:List, label:str, color=None, marker=None):
    """
    :param type_: type og graphs. Use graph_types.
    :param values: a list of [x,y] values
    :param label: name of the serie
    :param color: use colors or RGB/RGBA ([0.5, 0.2, 0.2, 0.1])
    :param marker: for simplicity use markers
    :return:
    """

    graph = Graph()

    graph.draw(type_, values, label, color, marker)

def draw_points(values: List, label: str, color=None, marker=None):
    draw(GraphTypes.points, values, label, color, marker)

def draw_line(values: List, label: str, color=None, marker=None):
    draw(GraphTypes.line, values, label, color, marker)

def create_contour_graph(plot=None, window_name:str=None, axis_labels:List[str]=None, x_values:List=None, y_values:List=None, z_values:List=None,
                         colors:List=None, colormap:str=None, alpha:float=None):

    from plotting.contourGraph import ContourGraph

    graph = ContourGraph(plot=plot, window_name=window_name, axis_labels=axis_labels,
                         x_values=x_values, y_values=y_values, z_values=z_values,
                         colors=colors, colormap=colormap, alpha=alpha )

    return graph
