from matplotlib import pyplot
from plotting import Graph

class GraphsContainer:



    def __init__(self, window_name:str, rows:int=1, columns:int=1):
        self.graphs = []

        self.figure = pyplot.figure()
        self.figure.canvas.set_window_title(window_name or "Graph")

        self.rows = rows
        self.columns = columns
        self.index = 1


    def add_graph(self, row, column):

        # manage the ridiculous way of matplotlib to define the plot position
        index = (row-1)*self.columns + column

        plot = self.figure.add_subplot(self.rows, self.columns, index) # row, column, index
        plot.legend(loc="best", fancybox=True)

        """
        if self.column % 3 == 0:
            self.column = 1
            self.row += 1
        else:
            self.column += 1        
        """

        self.index += 1

        return plot

    def xdraw(self):
        pyplot.ion()
        pyplot.show()


    def show(self, block=True):
        pyplot.ion()
        pyplot.show(block=block)
