from typing import List
import multiprocessing
from matplotlib import pyplot
import plotting

class Graph:

    def __init__(self, plot=None, window_name=None, axis_labels=None):
        """
        :param window_name: Name of the Window
        :param axis_labels: x and y axis labels
        """

        self.plot = plot

        if not self.plot:
            self.figure = pyplot.figure()
            self.figure.canvas.set_window_title(window_name or "Graph")
            self.plot = self.figure.add_subplot(111) # row, column, index


        self.series = []
        self.lines = []

        if axis_labels:
            self.plot.set_xlabel(axis_labels[0])
            self.plot.set_ylabel(axis_labels[1])

        #pyplot.ion()
        #pyplot.show()

    def draw_serie(self, values: List, label: str, color=None, marker=None):
        self.draw(plotting.graph_types.points, values, label, color, marker)

    def draw(self, type_:int, values:List, label:str, color=None, marker=None):
        """
        :param type_: type og graphs. Use GraphType.
        :param values: a list of [x,y] values
        :param label: name of the serie
        :param color: use Colors or RGB/RGBA ([0.5, 0.2, 0.2, 0.1])
        :param marker: for simplicity use Markers
        :return:
        """
        x = [record[0] for record in values]
        y = [record[1] for record in values]


        if type_ == plotting.graph_types.points:
            self.plot.scatter(x, y, label=label, c=color, marker=marker)
        if type_ == plotting.graph_types.line:
            self.plot.plot(x, y, label=label, c=color, marker=marker)

        self.series.append(label)

        pyplot.draw()

        self.plot.legend(loc="best", fancybox=True)


    def show(self, block=True):
        pyplot.ion()
        pyplot.show(block=block)


