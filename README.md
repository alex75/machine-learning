# Machine Learning

My study of Machine Learning.  
Practicing of code example on the book "Python Machine Learning" of Sebastian Raschka.
Personal experiments.  

Libraries used in the code:

- NumPy: [https://docs.scipy.org/doc/numpy-dev/user/quickstart.htmll](https://docs.scipy.org/doc/numpy-dev/user/quickstart.html)
- Pandas: [http://pandas.pydata.org/pandas-docs/stable/tutorials.html](http://pandas.pydata.org/pandas-docs/stable/tutorials.html)
- SciPy
- Matplotlib: [http://matplotlib.org/users/beginner.html](http://matplotlib.org/users/beginner.html)
- scikit-learn


## Definitions

Feature  
It is a property of the record (usually represents a column of the data matrix).  

Label  
It is the target value (class label).  


eta  
Learning rate. How much the weights should change in every iteration.  


## Framework and tools

### Python

- zip(): 


### Numpy

- array[:,x]
- arange()
- zeros()
- array()
- dot()
- min(), max()
- ravel()
- meshgrid()
- T

### Pandas

_read_csv(URL)_: returns a DataFrame  
_DataFrame.iloc(range, a, b)_: integer-location indexing. Docs [here](http://pandas.pydata.org/pandas-docs/version/0.17.0/generated/pandas.DataFrame.iloc.html).   
- range is something like x:y ("1:", "0:50", ":100")

 
 
 ### Matplotlib
 
 - markers: http://matplotlib.org/api/markers_api.html?highlight=marker
 - scatter()
 - contourf()